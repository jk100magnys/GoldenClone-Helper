/**
 * Functions for work with Prices
 */


class PriceHelper {

    /**
     * Constructor
     * @param storageHelper - instance of StorageHelper
     */
    constructor() {
        console.debug("PriceHelper::constructor");
        this.initGoodsList();
        this.storageHelper = new StorageHelper();

        var self = this;
        chrome.runtime.onMessage.addListener(function(request, sender) {
            if (request.action == "getPageSource") {
                self.pageSource = request.source;
                self.pageTitle  = request.title;

                console.debug("PriceHelper::constructor -> getSource", self.pageSource.length);

                if(!self.checkPage()) {
                    self.showLocationError();
                }
            }
        });

        $('a#refreshPrice').click(function(){
            self.refreshPrices();
        });

    }

    /**
     * Init instance
     */
    start() {

    }

    /**
     * Show error if page is wrong
     */
    showLocationError() {
        console.debug("PriceHelper::showLocationError");

        $('#notification').append("Пожалуйста, перейдите на страницу Торговой гильдии (вкладка 'Все')</br>");
        $('#notification').show();

        $('.nav-tabs > li > a').addClass('disabled');
    }

    /**
     * Check current page by title and size of table with prices
     * @returns {boolean}
     */
    checkPage() {
        // title = match "Торговая гильдия"
        // table = table#exchange_lots

        var result = false;
        console.debug("PriceHelper::checkPage");

        if(this.pageTitle.match(/Торговая гильдия/g)){
            result = true;
        } else {
            return false;
        }

        if($(this.pageSource).find('table#exchange_lots').length){
            result = true;
        } else {
            return false;
        }

        return result;
    }

    /**
     * Get page Source
     */
    getDomPage() {
        console.debug("PriceHelper::getDomPage");

        chrome.tabs.executeScript(null, {
            file: "/src/js/domLoader.js"
        }, function() {
            // If you try and inject into an extensions page or the webstore/NTP you'll get an error
            if (chrome.runtime.lastError) {
                console.log('There was an error injecting script : \n' + chrome.runtime.lastError.message);
            }
        });
    }

    /**
     * Refresh prices from page
     */
    refreshPrices() {
        console.debug("PriceHelper::refreshPrices");

        var prices = {};
        this.getDomPage();

        var self = this;
        $(this.pageSource).find('table#exchange_lots tr[id^=r_type_]').each(function(k, v){
            var row = {};

            // image
            row.image = "#"; //$(v).find("td:eq(0) > td > span").css('background-image');
            //nominal price
            row.nominal = $(v).find("td:eq(1) span").text().replace("(", "").replace(")","").replace(",",".").trim();
            $(v).find("td:eq(1) span").remove();
            // name
            row.name  = $(v).find("td:eq(1)").text().trim();
            // current price
            row.price = $(v).find("td:eq(2) span").text().replace(",",".").trim();

            prices[$(v).attr('id').replace("r_type_","")] = row;
        });

        this.setPrices(prices);
        this.showPrices();
    }


    /**
     * Get prices from local storage
     */
    getPrices() {
        console.debug("PriceHelper::getPrices");

        return this.storageHelper.storageCache;
    }

    /**
     * Set prices to local storage
     * @param prices
     */
    setPrices(prices) {
        console.debug("PriceHelper::setPrices");

        this.storageHelper.setData('prices', prices);
    }

    /**
     * show prices on the grid
     */
    showPrices(){
        console.debug("PriceHelper::showPrices");

        var html = "";
        var self = this;

        $('#priceTable tbody tr').remove();

        this.storageHelper.getData("prices", function(data){
            var _prices = data['prices'];
            console.debug("PriceHelper::showPrices", _prices);

            $.each(self.goodsList, function( index ) {

                var _class = "";
                console.log(parseFloat(_prices[index].price) + " <= " + parseFloat(_prices[index].nominal));

                if(parseFloat(_prices[index].price) <= parseFloat(_prices[index].nominal)) {
                    _class = "less";
                } else {
                    _class = "more";
                }

                $('#priceTable tbody').append(
                    "<tr class='"+ _class +"'>" +
                    "<td>" + _prices[index].image + "</td>" +
                    "<td>" + _prices[index].name + "</td>" +
                    "<td>" + _prices[index].nominal + "</td>" +
                    "<td>" + _prices[index].price + "</td>" +
                    "</tr>"
                );
            });

            $('#priceTable').show();
        });
    }


    /**
     * Fucking trash
     */
    initGoodsList() {
        this.goodsList = {
            1 : 'Дерево',
            2 : 'Злаковые',
            3 : 'Камень',
            4 : 'Железо',
            5 : 'Мука',
            6 : 'Хлеб',
            7 : 'Комбикорм',
            8 : 'Яйцо',
            9 : 'Молоко',
            10 : 'Свинина',
            11 : 'Говядина',
            109 : 'Курятина',
            142 : 'Сало',
            12 : 'Золотое яйцо',
            13 : 'Серебряное яйцо',
            14 : 'Сапфир',
            15 : 'Аметист',
            16 : 'Рубин',
            17 : 'Изумруд',
            18 : 'Алмаз',
            188 : 'Янтарь',
            189 : 'Янтарь с каплей крови дракона внутри',
            152 : 'Черный жемчуг',
            65 : 'Обработанное дерево',
            66 : 'Обработанный камень',
            67 : 'Обработанное железо',
            72 : 'Волшебная солнечная пластина',
            128 : 'Волшебная дождевая пластина',
            194 : 'Лунная пластина',
            129 : 'Соль',
            131 : 'Сера',
            132 : 'Селитра',
            133 : 'Древесный уголь',
            134 : 'Закладка пороха',
            136 : 'Гильза',
            137 : 'Патрон с солью',
            69 : 'Патрон боевой',
            70 : 'Пушечное ядро',
            78 : 'Люцерна',
            79 : 'Овес',
            82 : 'Турнепс',
            80 : 'Кормовая свекла',
            76 : 'Брюква',
            73 : 'Помидоры',
            74 : 'Картофель',
            81 : 'Капуста',
            77 : 'Баклажаны',
            75 : 'Лук',
            180 : 'Конопля',
            116 : 'Фундаментный блок',
            117 : 'Блок перекрытия',
            118 : 'Облицовочная плита',
            119 : 'Кирпич',
            120 : 'Брус',
            121 : 'Доска обрезная',
            122 : 'Монтажная рейка',
            123 : 'Вагонка',
            124 : 'Швеллер',
            125 : 'Двутавр',
            126 : 'Уголок',
            127 : 'Арматура',
            145 : 'Песок',
            105 : 'Купон Государственной фермы',
            155 : 'Нефть',
            156 : 'Нефтепродукт',
            177 : 'Глина',
            178 : 'Известняк',
            179 : 'Цемент',
            182 : 'Конопляное масло',
            185 : 'Такелажное оборудование для брига',
            186 : 'Такелажное оборудование для корвета',
            187 : 'Такелажное оборудование для линейного корабля'
        };
    }
}