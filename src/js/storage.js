/**
 *
 * Functions for work with Storage
 */

// See https://developer.chrome.com/apps/storage#type-StorageArea. We check
// for chrome.runtime.lastError to ensure correctness even when the API call
// fails.

class StorageHelper {

    /**
     * Constructor
     */
    constructor() {
        console.debug("StorageHelper::constructor");
        this.start();
    }

    /**
     * Init instance and cache data from storage
     */
    start() {
        console.debug("StorageHelper::start");
        this.refreshStorageCache();
    }

    /**
     * Callback for updating storageCache
     */
    refreshStorageCache() {
        var self = this;
        this.storageCache = this.getData(null, function(data){
            self.storageCache = data;
        });
        console.debug("StorageHelper::refreshStorageCache", this.storageCache);
    }

    /**
     * Saving data to local storage
     * @param key
     * @param value
     */
    setData(key, value) {
        var object = {};
        object[key] = value;

        console.debug("StorageHelper:setData", object);

        var self= this;
        chrome.storage.local.set(object, function(){
            //console.debug("Saving >> ", object);
            self.refreshStorageCache();
        });
    }

    /**
     * Getting data from local storage and save to cache
     * @param key
     * @param callback
     */
    getData(key, callback) {
        console.debug("StorageHelper::setData", key);

        //console.debug("Getting << ", key);
        chrome.storage.local.get(key, callback);
    }
}