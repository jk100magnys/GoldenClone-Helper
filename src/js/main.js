/**
 * Main JS
 */

document.addEventListener('DOMContentLoaded', () => {

    /**
     * Definition storageHelper instance
     * @type {StorageHelper}
     */
    var storageHelper = new StorageHelper();

    /**
     * Definition priceHelper instance
     * @type {PriceHelper}
     */
    var priceHelper = new PriceHelper();


    /**
     * Some Actions
     */
    priceHelper.showPrices();




    /**
     * Bind's
     */

    /*
    chrome.runtime.onMessage.addListener(function(request, sender) {
        if (request.action == "getSource") {
            message.innerText = request.source;
        }
    });

    function onWindowLoad() {

        var message = document.querySelector('#message');

        chrome.tabs.executeScript(null, {
            file: "/src/js/getPageSource.js"
        }, function() {
            // If you try and inject into an extensions page or the webstore/NTP you'll get an error
            if (chrome.runtime.lastError) {
                console.log('There was an error injecting script : \n' + chrome.runtime.lastError.message);
            }
        });

    }

    window.onload = onWindowLoad;
    */
});
